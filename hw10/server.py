import cherrypy

from nd_football_controller import FootballController


def CORS(): 
	cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
	cherrypy.response.headers["Access-Control-Allow-Origin"] = "GET, DELETE, PUT, POST"
	cherrypy.response.headers["Access-Control-Allow-Origin"] = "true"



def start_service():
	dispatcher = cherrypy.dispatch.RoutesDispatcher()
	conf = { 'global' : {'server.socket_host': 'localhost', 'server.socket_port': 51043,}, '/' : {'request.dispatch': dispatcher} }
	cherrypy.config.update(conf)
	app = cherrypy.tree.mount(None, config=conf)

	football_controller = FootballController()

	dispatcher.connect('get_year_data', '/ndfootball/:year', controller=football_controller, action = 'get_year_data', conditions=dict(method=['GET']))
	dispatcher.connect('get_year_week_data', '/ndfootball/:year/week', controller=football_controller, action='get_year_week_data', conditions=dict(method=['GET']))
	dispatcher.connect('get_all_data', '/ndfootball/', controller=football_controller, action='get_all_data', conditions=dict(method=['GET']))
	dispatcher.connect('get_year_win_loss_data', '/record/:year', controller=football_controller, action='get_year_win_loss_data', conditions=dict(method=['GET'])) 
	dispatcher.connect('put_week_data', '/ndfootball/:year/week', controller=football_controller, action='put_week_data', conditions=dict(method=['PUT']))
	dispatcher.connect('post_update', '/ndfootball/', controller=football_controller, action='post_update', conditions=dict(method=['POST']))
	dispatcher.connect('reload', '/ndfootball/reload', controller=football_controller, action='reload_data', conditions=dict(method=['GET']))
	dispatcher.connect('delete_week', '/ndfootball/:year/week', controller=football_controller, action = 'delete_week', conditions=dict(method=['DELETE']))
	dispatcher.connect('delete_year', '/ndfootball/:year', controller=football_controller, action = 'delete_year', conditions=dict(method=['DELETE']))
	dispatcher.connect('delete_all', '/ndfootball/', controller=football_controller, action = 'delete_all', conditions=dict(method=['DELETE'])) 

	cherrypy.quickstart(app)

	conf = {
			'global' : {
				'server.socket_host' : 'localhost', 'student04.cse.nd.edu', 'server.socket_port' : 51043, 
				}, 
			'/' : {
				'request.dispatch' : dispatcher, 
				'tools.CORS.on' : True, 
				}
		}

	cherrypy.config.update(conf) 
	app = cherrypy.tree.mount(None, config=conf)
	cherrypy.quickstart(app)

class optionsController: 
	def OPTIONS(self, *args, **kwargs):
		return ""

if __name__ == '__main__':
	cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS) 
    start_service()
