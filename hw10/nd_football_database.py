import json

class FootballDatabase:
    
    def __init__(self):
        self.football_data = dict()

    def load_data(self, data_file):
        json_data = open(data_file)
        full_data = json.load(json_data)

        for data_object in full_data:
            if data_object["season"] not in self.football_data:
                self.football_data[data_object["season"]] = dict()

            self.football_data[data_object["season"]][data_object["week"]] = dict()
            if data_object["home_team"] == "Notre Dame":
                self.football_data[data_object["season"]][data_object["week"]]["nd_score"] = data_object["home_points"]
                self.football_data[data_object["season"]][data_object["week"]]["opponent"] = data_object["away_team"]
                self.football_data[data_object["season"]][data_object["week"]]["opponent_score"] = data_object["away_points"]
                self.football_data[data_object["season"]][data_object["week"]]["is_home"] = True
            else:
                self.football_data[data_object["season"]][data_object["week"]]["nd_score"] = data_object["away_points"]
                self.football_data[data_object["season"]][data_object["week"]]["opponent"] = data_object["home_team"]
                self.football_data[data_object["season"]][data_object["week"]]["opponent_score"] = data_object["home_points"]
                self.football_data[data_object["season"]][data_object["week"]]["is_home"] = False

        json_data.close()

    def get_year(self, year):
        year_data = self.football_data[year]
        data = []

        for key, value in year_data.items():

            entry = {}
            week = str(key)

            if value is not None:
                entry['season'] = year
                entry['week']   = week
                entry['opponent'] = value['opponent']
                entry['nd_score'] = value['nd_score']
                entry['opponent_score'] = value['opponent_score']
                data.append(entry)
                
        return data
    
    def get_year_week(self, year, week):
        year_data = self.football_data[year]
        data = []

        for key, value in year_data.items():

            entry = {}

            if value is not None and str(key) == week:
                entry['season'] = year
                entry['week']   = str(key)
                entry['opponent'] = value['opponent']
                entry['nd_score'] = value['nd_score']
                entry['opponent_score'] = value['opponent_score']
                data.append(entry)

        if not data:
            data.append("No game data for week " + str(week))
                
        return data
    
    def get_all_data(self):

        full_data = []
        
        for season, weeks in self.football_data.items():
            year = dict()
            games = []

            year["season"] = str(season)
            year["games"] = games

            for week, game_details in weeks.items():
                game = dict()

                game["week"] = str(week)
                game['opponent'] = game_details["opponent"]
                game['nd_score'] = game_details['nd_score']
                game['opponent_score'] = game_details['opponent_score']

                games.append(game)

            full_data.append(year)
        
        return full_data

    def get_year_win_loss_data (self, year):
        
        year_data = self.football_data[year]
        wins = 0
        losses = 0

        for week, game_data in year_data.items():
            if game_data['nd_score'] > game_data['opponent_score']:
                wins += 1
            else:
                losses += 1

        return str(wins) + " wins and " + str(losses) + " losses."

    def put_week_data(self, year, week, data):
        self.football_data[year][week]['nd_score'] = data['nd_score']
        self.football_data[year][week]['opponent'] = data['opponent']
        self.football_data[year][week]['opponent_score'] = data['opponent_score']
        self.football_data[year][week]['home_game'] = data['home_game']

    def post_game(self, data):

        if data["season"] not in self.football_data:
                self.football_data[data["season"]] = dict()
            
        self.football_data[data["season"]][data["week"]] = dict()

        self.football_data[data["season"]][data["week"]]['nd_score'] = data['nd_score']
        self.football_data[data["season"]][data["week"]]['opponent'] = data['opponent']
        self.football_data[data["season"]][data["week"]]['opponent_score'] = data['opponent_score']
        self.football_data[data["season"]][data["week"]]['home_game'] = data['home_game']

    def delete_week(self, year, week):

        del self.football_data[year][week]

    def delete_year(self, year):

        del self.football_data[year]

    def delete_all(self):

        self.football_data.clear()