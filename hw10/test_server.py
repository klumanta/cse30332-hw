import unittest 
import requests 
import json 

class TestNDFootball(unittest.TestCase): 
	SITE_URL = 'http://localhost:51043'
	FB_URL = SITE_URL + '/ndfootball/'
	RELOAD_URL = FB_URL + '/reload'
	RECORD_URL = SITE_URL + '/record/'

	def reset_data(self): 
		r = requests.delete(self.FB_URL)
		r = requests.get(self.RELOAD_URL)


	def is_json(self, response):
		try: 
			json.loads(response) 
			return True 
		except ValueError: 
			return False

	# TODO: Test get route?? 

	def test_get_year_data(self):	
		self.reset_data()

		data_result = [{'season': 2019, 'week': '1', 'opponent': 'Louisville', 'nd_score': 35, 'opponent_score': 17}, {'season': 2019, 'week': '3', 'opponent': 'New Mexico', 'nd_score': 66, 'opponent_score': 14}, {'season': 2019, 'week': '4', 'opponent': 'Georgia', 'nd_score': 17, 'opponent_score': 23}, {'season': 2019, 'week': '5', 'opponent': 'Virginia', 'nd_score': 35, 'opponent_score': 20}, {'season': 2019, 'week': '6', 'opponent': 'Bowling Green', 'nd_score': 52, 'opponent_score': 0}, {'season': 2019, 'week': '7', 'opponent': 'USC', 'nd_score': 30, 'opponent_score': 27}, {'season': 2019, 'week': '9', 'opponent': 'Michigan', 'nd_score': 14, 'opponent_score': 45}, {'season': 2019, 'week': '10', 'opponent': 'Virginia Tech', 'nd_score': 21, 'opponent_score': 20}, {'season': 2019, 'week': '11', 'opponent': 'Duke', 'nd_score': 38, 'opponent_score': 7}, {'season': 2019, 'week': '12', 'opponent': 'Navy', 'nd_score': 52, 'opponent_score': 20}, {'season': 2019, 'week': '13', 'opponent': 'Boston College', 'nd_score': 40, 'opponent_score': 7}, {'season': 2019, 'week': '14', 'opponent': 'Stanford', 'nd_score': 45, 'opponent_score': 24}]

		r = requests.get(self.FB_URL + '2019')
		self.assertTrue(self.is_json(r.content.decode()))
		response = json.loads(r.content.decode())
		self.assertEqual(response['result'], 'success')
		self.assertEqual(response['games'], data_result)


	def test_get_all_data(self):
		self.reset_data()

		data_result = [{'season': '2019', 'games': [{'week': '1', 'opponent': 'Louisville', 'nd_score': 35, 'opponent_score': 17}, {'week': '3', 'opponent': 'New Mexico', 'nd_score': 66, 'opponent_score': 14}, {'week': '4', 'opponent': 'Georgia', 'nd_score': 17, 'opponent_score': 23}, {'week': '5', 'opponent': 'Virginia', 'nd_score': 35, 'opponent_score': 20}, {'week': '6', 'opponent': 'Bowling Green', 'nd_score': 52, 'opponent_score': 0}, {'week': '7', 'opponent': 'USC', 'nd_score': 30, 'opponent_score': 27}, {'week': '9', 'opponent': 'Michigan', 'nd_score': 14, 'opponent_score': 45}, {'week': '10', 'opponent': 'Virginia Tech', 'nd_score': 21, 'opponent_score': 20}, {'week': '11', 'opponent': 'Duke', 'nd_score': 38, 'opponent_score': 7}, {'week': '12', 'opponent': 'Navy', 'nd_score': 52, 'opponent_score': 20}, {'week': '13', 'opponent': 'Boston College', 'nd_score': 40, 'opponent_score': 7}, {'week': '14', 'opponent': 'Stanford', 'nd_score': 45, 'opponent_score': 24}]}, {'season': '2018', 'games': [{'week': '1', 'opponent': 'Michigan', 'nd_score': 24, 'opponent_score': 17}, {'week': '2', 'opponent': 'Ball State', 'nd_score': 24, 'opponent_score': 16}, {'week': '3', 'opponent': 'Vanderbilt', 'nd_score': 22, 'opponent_score': 17}, {'week': '4', 'opponent': 'Wake Forest', 'nd_score': 56, 'opponent_score': 27}, {'week': '5', 'opponent': 'Stanford', 'nd_score': 38, 'opponent_score': 17}, {'week': '6', 'opponent': 'Virginia Tech', 'nd_score': 45, 'opponent_score': 23}, {'week': '7', 'opponent': 'Pittsburgh', 'nd_score': 19, 'opponent_score': 14}, {'week': '9', 'opponent': 'Navy', 'nd_score': 44, 'opponent_score': 22}, {'week': '10', 'opponent': 'Northwestern', 'nd_score': 31, 'opponent_score': 21}, {'week': '11', 'opponent': 'Florida State', 'nd_score': 42, 'opponent_score': 13}, {'week': '12', 'opponent': 'Syracuse', 'nd_score': 36, 'opponent_score': 3}, {'week': '13', 'opponent': 'USC', 'nd_score': 24, 'opponent_score': 17}]}]

		r = requests.get(self.FB_URL)
		self.assertTrue(self.is_json(r.content.decode()))
		response = json.loads(r.content.decode())
		self.assertEqual(response['result'], 'success')
		self.assertEqual(response['data'], data_result)
	

	def test_get_week_data(self):
		self.reset_data()

		r = requests.get(self.FB_URL + '2019/' + '3')
		self.assertTrue(self.is_json(r.content.decode()))
		response = json.loads(r.content.decode())
		self.assertEqual(response['result'], 'success')
		self.assertTrue(response['year'])
		self.assertTrue(response['year'][2019]['week'], '3')

	def test_get_year_win_loss_data(self):
		self.reset_data()

		data_result = '10 wins and 2 losses.'
		r = requests.get(self.RECORD_URL + '2019')
		self.assertTrue(self.is_json(r.content.decode()))
		response = json.loads(r.content.decode())
		self.assertEqual(response['result'], 'success')
		self.assertEqual(response['record'], data_result)

	def test_put_week_data(self):
		self.reset_data() 

		y = {'nd_score': 20, 'opponent': 'Michigan', 'opponent_score': 10, 'home_game': True}
		data_result = [{'season': 2021, 'week': 1, 'nd_score': 20, 'opponent': 'Michigan', 'opponent_score': 10, 'home_game': True}]

		r = requests.put(self.FB_URL + '2019/' + '1', data = json.dumps(y))
		self.assertTrue(self.is_json(r.content.decode()))
		response = json.loads(r.content.decode())
		self.assertEqual(response['result'], 'success') 

		r = requests.get(self.FB_URL + '2019/' + '1')
		self.assertTrue(self.is_json(r.content.decode()))
		response = json.loads(r.content.decode())
		self.assertEqual(response['result'], 'success')
		self.assertEqual(response['game'], data_result)
		


	def test_post_update(self): 
		self.reset_data() 

		y = {'season': 2021, 'week': 1, 'nd_score': 20, 'opponent': 'Michigan', 'opponent_score': 10, 'home_game': True}
		r = requests.post(self.FB_URL, data = json.dumps(y))
		self.assertTrue(self.is_json(r.content.decode()))
		response = json.loads(r.content.decode())
		self.assertEqual(response['result'], 'success')

		data_result = [{'season': 2021, 'week': '1', 'opponent': 'Michigan', 'nd_score': 20, 'opponent_score': 10}]

		r = requests.get(self.FB_URL + '2021')
		self.assertTrue(self.is_json(r.content.decode()))
		response = json.loads(r.content.decode())
		self.assertEqual(response['result'], 'success')
		self.assertEqual(response['games'], data_result)

	def test_delete_year(self): 
		self.reset_data()

		r = requests.delete(self.FB_URL + '2019')
		self.assertTrue(self.is_json(r.content.decode()))
		response = json.loads(r.content.decode())
		self.assertEqual(response['result'], 'success') 

		r = requests.get(self.FB_URL + '2019')
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		response = json.loads(r.content.decode('utf-8'))
		self.assertEqual(response['result'], 'error')
		self.assertEqual(response['message'], 'key not found')

	def test_delete_week(self):
		self.reset_data()

		r = requests.delete(self.FB_URL + '2019/' + '3')
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		response = json.loads(r.content.decode('utf-8'))
		self.assertEqual(response['result'], 'success') 

		r = requests.get(self.FB_URL + '2019/' + '3')
		self.assertTrue(self.is_json(r.content.decode('utf-8')))
		response = json.loads(r.content.decode('utf-8'))
		self.assertEqual(response['result'], 'error')
		self.assertEqual(response['message'], 'key not found')

	def test_delete_all(self): 
		self.reset_data() 

		r = requests.delete(self.FB_URL)
		self.assertTrue(self.is_json(r.content.decode()))
		response = json.loads(r.content.decode())
		self.assertEqual(response['result'], 'success') 

		r = requests.get(self.FB_URL + '2019')
		self.assertTrue(self.is_json(r.content.decode()))
		response = json.loads(r.content.decode())
		self.assertEqual(response['result'], 'error')
		self.assertEqual(response['message'], 'key not found')




if __name__ == "__main__":
	unittest.main()
