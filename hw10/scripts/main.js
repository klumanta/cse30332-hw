// Molly Doyle - mdoyle22
// Kenan Lumantas - klumanta

console.log('page load - entered main.js for js-other api');

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('entered get form info');
    var name = document.getElementById("name-text").value;
    makeNetworkCalltoDogApi(name);
}

function makeNetworkCalltoDogApi(name){
    console.log('entered makeNetworkCalltoDogApi');
    var url = "https://dog.ceo/api/breeds/image/random";
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    // set up onload - triggered when nw response is received
    // must be defined before making the network call
    xhr.onload = function(e){
        console.log('network response received' + xhr.responseText);
        updateDogWithResponse(name, xhr.responseText);
    }

    // set up onerror - triggered if nw response is error response
    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null);
}

function updateDogWithResponse(name, response_text){
    // extract json info from response
    var response_json = JSON.parse(response_text);
    // update label with it
    var label1 = document.getElementById('response-line1');

    if(response_json['message'] == null){
        label1.innerHTML = 'Apologies, we could not find a dog for you.';
    } else{
        label1.innerHTML = name + ' here is a dog to brighten up your day.';
        var img = document.getElementById('dog-img');
        img.src = response_json['message'];
    }

    makeNetworkCalltoCatApi(name);
}

function makeNetworkCalltoCatApi(name){
    console.log('entered makeNetworkCalltoFootballApi');

    var label2 = document.getElementById('response-line2');
    label2.innerHTML = name + ' if you don\'t like dogs, here is a cat for you.';

    var url = "https://placekitten.com/" + getRndInteger(600, 1200) + '/' + getRndInteger(600, 1200);
    var img = document.getElementById('cat-img');
    img.src = url;
}

function getRndInteger(minimum, maximum) {
    return Math.floor(Math.random() * (maximum - minimum)) + minimum;
}