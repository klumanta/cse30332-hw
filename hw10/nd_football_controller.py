import json
import re #?

import cherrypy

from nd_football_database import FootballDatabase


class FootballController(object):
	def __init__(self):
		self.fdb = FootballDatabase()
		self.fdb.load_data("nd_data.json")

	def get_year_data(self, year): # display data for year inputted by user 

		year = int(year)

		try: 
			year_data = self.fdb.get_year(year)
			output = {'result': 'success', 'games': year_data}

		except KeyError as ex:
			output = {'result': 'error', 'message': 'key not found'}

		except ValueError as ex:
			output = {'result': 'success', 'message': 'incorrect value type'}

		except Exception as ex:
			output = {'result': 'success', 'message': str(ex)}		
		
		return json.dumps(output)

	# displays data for week inputted by user 
	def get_year_week_data(self, year, *args):		
		
		try: 
			year = int(year)
			week = int(args[0])
			week_data = self.fdb.get_year_week(year, week)

			if not week_data:
				week_data = 'No game associated with that year and week'

			output = {'result': 'success', 'game': week_data}
		except KeyError as ex:
			output = {} 
			output['result'] = 'error' 
			output['message'] = 'issue with year ' + str(ex) 
			output['game'] = 'No data returned'
		except Exception as ex:
			output = {}  
			output['result'] = 'error'
			output['message'] = str(ex) 
			output['game'] = 'No data returned'

		return json.dumps(output)

	def get_all_data(self):

		full_data = self.fdb.get_all_data()

		if not len(full_data):
			full_data.append('No Data Found')

		output = {'result': 'success', 'data': full_data}

		return json.dumps(output)

	def get_year_win_loss_data(self, year):
		
		record = ''
		output = {'result': 'success', 'record': record}

		try: 
			record = self.fdb.get_year_win_loss_data(int(year))
			output = {'result': 'success', 'record': record}

			if not record:
				record = 'No record data found'
		except KeyError as ex: 
			output['result'] = 'error' 
			output['message'] = 'issue with year ' + str(ex) 
			output['record'] = 'No data returned'
		except Exception as ex: 
			output['result'] = 'error'
			output['message'] = str(ex) 
			output['record'] = 'No data returned'

		return json.dumps(output)

	# updates a game that already exists for a given week in the year 
	def put_week_data(self, year, *args): 
		output = {'result':'success'}

		try:
			year = int(year)
			week = int(args[0]) 
			data_str = cherrypy.request.body.read().decode()
			data_json = json.loads(data_str)  
			self.fdb.put_week_data(year, week, data_json)
		except KeyError as ex: 
			output['result'] = 'error' 
			output['message'] = 'issue with year or week ' + str(ex) 
		except Exception as ex: 
			output['result'] = 'error'
			output['message'] = str(ex) 

		return json.dumps(output)

	def post_update(self):
		output = {'result':'success'}

		try: 
			data_str = cherrypy.request.body.read() 
			data_json = json.loads(data_str) 
			self.fdb.post_game(data_json)
		except KeyError as ex: 
			output['result'] = 'error' 
			output['message'] = 'issue with input data: ' + str(ex)
		except Exception as ex: 
			output['result'] = 'error'
			output['message'] = str(ex) 
		
		return json.dumps(output) 

	# deletes a week --> very not finished 
	def delete_week(self, year, *args): 
		output = {'result':'success'}
		
		year = str(year)
		week = str(args[0])

		self.fdb.delete_week(year, week)

		return json.dumps(output)


	# deletes a year 
	def delete_year(self, year): 
		output = {'result':'success'}

		self.fdb.delete_year(int(year))

		return json.dumps(output) 

	# deletes all data 
	def delete_all(self): 
		output = {'result' : 'success'} 

		self.fdb.delete_all()

		return json.dumps(output) 

	def reload_data(self):
		output = {'result' : 'success'} 

		self.fdb.load_data("nd_data.json")

		return json.dumps(output) 